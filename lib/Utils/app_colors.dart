import 'dart:ui';

var buttonColor = const Color(0xFF006837);
var borderColor = const Color(0xFFdadada);
var hintColor = const Color(0xFF888888);
var shadowColor=const Color(0xFFD3D3D3);
var bgColor=const Color(0xFFf4f4fc);
var cardColor = const Color(0xFFF1F1F1);
var lightGreen = const Color(0xFF389A48);