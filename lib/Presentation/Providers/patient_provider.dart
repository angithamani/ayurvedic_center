import 'package:ayurvedic_center_demo/Domain/Models/PatientModel.dart';
import 'package:flutter/cupertino.dart';

class PatientProvider extends ChangeNotifier {
  List<PatientsModel> patientsList = [];
  List branches = [];
  String selectedBranch = '';
  String selectedTreatment = '';
  List treatments = [];

  void getPatients(value) {
    patientsList.add(value);
    notifyListeners();
  }

  void getBranches(value) {
    branches.add(value);
    selectedBranch = branches[0]['name'];
    notifyListeners();
  }
  void getTreatments(value) {
    treatments.add(value);
    selectedTreatment = treatments[0]['name'];

    notifyListeners();
  }
}
