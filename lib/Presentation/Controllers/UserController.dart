import 'dart:convert';

import 'package:ayurvedic_center_demo/Domain/Models/PatientModel.dart';
import 'package:ayurvedic_center_demo/Presentation/Providers/patient_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:ayurvedic_center_demo/Domain/Repository/user_repository.dart'
    as user_repo;
import 'package:ayurvedic_center_demo/Presentation/Elements/app_service.dart'
    as app_service;
import 'package:provider/provider.dart';

class UserController extends ControllerMVC {
  factory UserController() {
    _this = UserController._();
    return _this;
  }

  static UserController _this = UserController._();

  BuildContext? get context => con.state?.context;

  UserController._();

  static UserController get con => _this;

  Future getPatientsList() async {
    Provider.of<PatientProvider>(context!, listen: false).patientsList = [];
    app_service.getCurrentUser().then((user) {
      if (user != null) {
        user_repo.getPatientsList(user).then((value) {
          if (value != null && value['patient'].length > 0) {
            for (var data in value['patient']) {
              Provider.of<PatientProvider>(context!, listen: false)
                  .getPatients(patientsModelFromJson(json.encode(data)));
            }
          }
        });
      }
    });
  }

  Future getBranches() async {
    Provider.of<PatientProvider>(context!, listen: false).branches = [];
    Provider.of<PatientProvider>(context!, listen: false).selectedBranch = '';

    app_service.getCurrentUser().then((user) {
      if (user != null) {
        user_repo.getBranchList(user).then((value) {
          if (value != null && value['branches'].length > 0) {
            for (var data in value['branches']) {
              Provider.of<PatientProvider>(context!, listen: false)
                  .getBranches(data);
            }
          }
        });
      }
    });
  }

  Future getTreatments() async {
    Provider.of<PatientProvider>(context!, listen: false).treatments = [];
    Provider.of<PatientProvider>(context!, listen: false).selectedTreatment = '';
    app_service.getCurrentUser().then((user) {
      if (user != null) {
        user_repo.getTreatmentList(user).then((value) {
          if (value != null && value['treatments'].length > 0) {
            for (var data in value['treatments']) {
              Provider.of<PatientProvider>(context!, listen: false)
                  .getTreatments(data);
            }
          }
        });
      }
    });
  }
}
