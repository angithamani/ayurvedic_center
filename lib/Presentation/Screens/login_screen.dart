import 'package:ayurvedic_center_demo/Presentation/Screens/patients_listing.dart';
import 'package:ayurvedic_center_demo/Utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:ayurvedic_center_demo/Domain/Repository/user_repository.dart'
    as userRepository;
import 'package:ayurvedic_center_demo/Presentation/Elements/app_service.dart'
    as app_service;

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool loginLoader = false;
  @override
  void initState() {
    loginLoader = false;
    emailController.text = '';
    passwordController.text = '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/Frame.png',
              width: double.infinity,
              height: screenHeight * .25,
              fit: BoxFit.cover,
            ),
            Container(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth * .07,
                    vertical: screenHeight * .01),
                child: const Text(
                  'Login Or Register To Book Your Appointments',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                )),
            SizedBox(
              height: screenHeight * .05,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: screenWidth * .1),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Email'),
                  SizedBox(
                    height: screenHeight * .01,
                  ),
                  TextFormField(
                    controller: emailController,
                    validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                              .hasMatch(value)) {
                        return 'Enter a valid E-mail';
                      }

                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(12),
                      labelText: 'Enter your email',
                      fillColor: cardColor,filled: true,
                      labelStyle: TextStyle(fontSize: 13, color: hintColor),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          borderSide: BorderSide(
                            color: borderColor,
                            width: 1,
                          )),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: borderColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: borderColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * .05,
                  ),
                  const Text('Password'),
                  SizedBox(
                    height: screenHeight * .01,
                  ),
                  TextFormField(
                    controller: passwordController,
                    // style: styleWidget(),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Please enter a password';
                      }

                      return null;
                    },

                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(12),
                      labelText: 'Enter password',
                      labelStyle: TextStyle(fontSize: 13, color: hintColor),
                      fillColor: cardColor,filled: true,
                      border: OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            Radius.circular(5.0),
                          ),
                          borderSide: BorderSide(
                            color: borderColor,
                            width: 1,
                          )),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: borderColor,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0),
                        ),
                        borderSide: BorderSide(
                          width: 1,
                          color: borderColor,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight * .05,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth * .01,
                        vertical: screenHeight * .018),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: buttonColor,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: buttonColor),
                    ),
                    child: GestureDetector(
                        onTap: ()  async {

                          setState(() {
                            loginLoader = true;
                          });
                            await userRepository
                                .postLogin(emailController.text, passwordController.text)
                                .then((value) {
                              if (value != null) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content: Text('${value['message']}')));
                                if (value['status'] == true) {
                                  app_service.setCurrentUser(value);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                          const PatientsListingScreen()));
                                  emailController.text = '';
                                  passwordController.text = '';
                                }
                              }
                           setState(() {
                             loginLoader = false;
                           });

                          });


                        },
                        child:   Center(
                            child:!loginLoader?const Text('Login',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17)):CircularProgressIndicator(color: Colors.white,)
                        )),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
