import 'package:ayurvedic_center_demo/Presentation/Controllers/UserController.dart';
import 'package:ayurvedic_center_demo/Presentation/Providers/patient_provider.dart';
import 'package:ayurvedic_center_demo/Utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:ayurvedic_center_demo/Presentation/Elements/app_service.dart'
    as app_service;
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends StateMVC<RegisterScreen> {
  _RegisterScreenState() : super(UserController()) {
    _con = UserController.con;
  }

  late UserController _con;

  Widget textFormWidget(
      {bool isName = false, bool isWhatsApp = false, bool isAddress = false}) {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(12),
        labelText: isName
            ? 'Enter Your Name'
            : isWhatsApp
                ? 'Enter Your WhatsApp Number'
                : isAddress
                    ? 'Enter Your full Address'
                    : '',
        labelStyle: TextStyle(fontSize: 13, color: hintColor),
        fillColor: cardColor,
        filled: true,
        border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(5.0),
            ),
            borderSide: BorderSide(
              color: borderColor,
              width: 1,
            )),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
          borderSide: BorderSide(
            width: 1,
            color: borderColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
          borderSide: BorderSide(
            width: 1,
            color: borderColor,
          ),
        ),
      ),
    );
  }

  final List<String> items = ['Kozhikode', 'Thrissure', 'Eranakulam'];
  String selectedItem = 'Kozhikode';

  @override
  void initState() {
    _con.getBranches();
    _con.getTreatments();
    super.initState();
  }

  Widget quantityWidget(
    screenWidth,
    screenHeight,
    int id,
  ) {
    return Row(
      children: [
        Container(
          width: screenWidth * .3,
          padding: EdgeInsets.symmetric(
              //horizontal: screenWidth * .03,
              vertical: screenHeight * .012),
          decoration: BoxDecoration(
            color: cardColor,
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: hintColor),
          ),
          child: Center(child: Text(id == 0 ? 'Male' : 'Female')),
        ),
        Spacer(),
        GestureDetector(
          onTap: (){

          },
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth * .05, vertical: screenHeight * .017),
            decoration: BoxDecoration(
                color: buttonColor,
                borderRadius: BorderRadius.circular(80),
                border: Border.all(color: buttonColor)),
            child: Text(
              '-',
              style: TextStyle(
                  color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        SizedBox(
          width: screenWidth * .01,
        ),
        Container(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth * .05, vertical: screenHeight * .017),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(7),
              border: Border.all(color: borderColor)),
          child: Text(
            '3',
            style: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ),
        SizedBox(
          width: screenWidth * .01,
        ),
        GestureDetector(
          onTap: (){

          },
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth * .05, vertical: screenHeight * .017),
            decoration: BoxDecoration(
                color: buttonColor,
                borderRadius: BorderRadius.circular(80),
                border: Border.all(color: buttonColor)),
            child: Text(
              '+',
              style: TextStyle(
                  color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }

  void _showDialog(screenWidth, screenHeight) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return

            // StatefulBuilder(builder: (context, setState) {
            // return
            AlertDialog(
          surfaceTintColor: Colors.white,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0))),
          title: const Text(
            'Choose Treatment',
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.w800),
          ),
          insetPadding: EdgeInsets.symmetric(horizontal: screenWidth * .04),
          contentPadding: EdgeInsets.symmetric(
              horizontal: screenWidth * .02, vertical: screenHeight * .01),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Consumer<PatientProvider>(
                  builder: (context, treatmentList, child) {
                return Container(
                    //width: double.infinity,
                    decoration: BoxDecoration(
                        color: cardColor,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: borderColor)),
                    height: screenHeight * .06,
                    padding:
                        EdgeInsets.symmetric(horizontal: screenWidth * .02),
                    child: Center(
                        child: DropdownButtonFormField<String>(
                      isExpanded: true,
                      value: treatmentList.selectedTreatment,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      decoration: InputDecoration.collapsed(hintText: ''),
                      items: treatmentList.treatments
                          .map<DropdownMenuItem<String>>((value) {
                        return DropdownMenuItem(
                            value: value['name'], child: Text(value['name']));
                      }).toList(),
                      onChanged: (String? newValue) {
                        treatmentList.selectedTreatment;
                      },
                    )));
              }),
              SizedBox(
                height: screenHeight * .03,
              ),
              const Text(
                'Add Patients',
                style: TextStyle(color: Colors.black),
              ),
              SizedBox(
                height: screenHeight * .01,
              ),
              quantityWidget(screenWidth, screenHeight, 0),
              SizedBox(
                height: screenHeight * .03,
              ),
              quantityWidget(screenWidth, screenHeight, 1),
              SizedBox(
                height: screenHeight * .04,
              ),
              GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth * .01,
                      vertical: screenHeight * .018),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: buttonColor,
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: buttonColor),
                  ),
                  child: const Center(
                      child: Text(
                    'Save',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        fontSize: 16),
                  )),
                ),
              ),
              SizedBox(
                height: screenHeight * .04,
              ),
            ],
          ),
        );
        // });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth * .05, vertical: screenHeight * .03),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Name'),
              SizedBox(
                height: screenHeight * .01,
              ),
              textFormWidget(isName: true),
              SizedBox(
                height: screenHeight * .025,
              ),
              const Text('Whatsapp Number'),
              SizedBox(
                height: screenHeight * .01,
              ),
              textFormWidget(isWhatsApp: true),
              SizedBox(
                height: screenHeight * .025,
              ),
              const Text('Address'),
              SizedBox(
                height: screenHeight * .01,
              ),
              textFormWidget(isAddress: true),
              SizedBox(
                height: screenHeight * .025,
              ),
              Text('Location'),
              SizedBox(
                height: screenHeight * .01,
              ),
              Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: cardColor,
                      borderRadius: BorderRadius.circular(5),
                      border: Border.all(color: borderColor)),
                  height: screenHeight * .06,
                  padding: EdgeInsets.symmetric(horizontal: screenWidth * .02),
                  child: Center(
                      child: DropdownButtonFormField<String>(
                    value: selectedItem,
                    icon: const Icon(Icons.keyboard_arrow_down),
                    decoration: InputDecoration.collapsed(hintText: ''),
                    items: items.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem(value: value, child: Text(value));
                    }).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedItem = newValue!;
                      });
                    },
                  ))),
              SizedBox(
                height: screenHeight * .025,
              ),
              const Text('Branch'),
              SizedBox(
                height: screenHeight * .01,
              ),
              Consumer<PatientProvider>(builder: (context, branchList, child) {
                return Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: cardColor,
                        borderRadius: BorderRadius.circular(5),
                        border: Border.all(color: borderColor)),
                    height: screenHeight * .06,
                    padding:
                        EdgeInsets.symmetric(horizontal: screenWidth * .02),
                    child: Center(
                        child: DropdownButtonFormField<String>(
                      value: branchList.selectedBranch,
                      icon: const Icon(Icons.keyboard_arrow_down),
                      decoration: InputDecoration.collapsed(hintText: ''),
                      items: branchList.branches
                          .map<DropdownMenuItem<String>>((value) {
                        return DropdownMenuItem(
                            value: value['name'], child: Text(value['name']));
                      }).toList(),
                      onChanged: (String? newValue) {
                        branchList.selectedBranch;
                      },
                    )));
              }),
              SizedBox(
                height: screenHeight * .025,
              ),
              GestureDetector(
                onTap: () {
                  _showDialog(screenWidth, screenHeight);
                },
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth * .02,
                      vertical: screenHeight * .02),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: lightGreen.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: const Center(
                      child: Text(
                    ' + Add Treatments',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  )),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
