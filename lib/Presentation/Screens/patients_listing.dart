import 'package:ayurvedic_center_demo/Domain/Models/PatientModel.dart';
import 'package:ayurvedic_center_demo/Domain/Repository/user_repository.dart';
import 'package:ayurvedic_center_demo/Presentation/Controllers/UserController.dart';
import 'package:ayurvedic_center_demo/Presentation/Providers/patient_provider.dart';
import 'package:ayurvedic_center_demo/Presentation/Screens/register_screen.dart';
import 'package:ayurvedic_center_demo/Utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:provider/provider.dart';

class PatientsListingScreen extends StatefulWidget {
  const PatientsListingScreen({super.key});

  @override
  _PatientsListingScreenState createState() => _PatientsListingScreenState();
}

class _PatientsListingScreenState extends StateMVC<PatientsListingScreen> {
  _PatientsListingScreenState() : super(UserController()) {
    _con = UserController.con;
  }

  late UserController _con;
  @override
  void initState() {
    _con.getPatientsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Consumer<PatientProvider>(builder: (context, provider, child) {
          return Container(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth * .05, vertical: screenHeight * .03),
            child: Column(
              children:
                  provider.patientsList.map<Widget>((PatientsModel patient) {
                return Container(
                  width: double.infinity,
                  margin: EdgeInsets.only(bottom: screenHeight * .03),
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth * .03,
                      vertical: screenHeight * .02),
                  decoration: BoxDecoration(
                    color: cardColor,
                    border: Border.all(color: borderColor),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(patient.name),
                      SizedBox(
                        height: screenHeight * .01,
                      ),
                      Text(patient.patientdetailsSet[0].treatmentName),
                      SizedBox(
                        height: screenHeight * .01,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.calendar_today_outlined,
                            color: Colors.red,
                            size: 15,
                          ),
                          SizedBox(
                            width: screenWidth * .015,
                          ),
                          Text(patient.dateNdTime.toString()),
                          SizedBox(
                            width: screenWidth * .05,
                          ),
                          Icon(
                            Icons.people,
                            color: Colors.red,
                            size: 15,
                          ),
                          SizedBox(
                            width: screenWidth * .015,
                          ),
                          Text(patient.user)
                        ],
                      ),
                      SizedBox(
                        height: screenHeight * .01,
                      ),
                      Divider(
                        color: hintColor,
                      ),
                      SizedBox(
                        height: screenHeight * .01,
                      ),
                      Row(
                        children: [
                          Text('View Booking Details'),
                          Spacer(),
                          Icon(
                            Icons.arrow_forward_ios_outlined,
                            color: buttonColor,
                            size: 18,
                          )
                        ],
                      ),
                    ],
                  ),
                );
              }).toList(),
            ),
          );
        }),
      ),
      bottomNavigationBar: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                  const RegisterScreen()));
        },
        child: Container(
          height: screenHeight * .08,
          margin: EdgeInsets.symmetric(
              horizontal: screenWidth * .05, vertical: screenHeight * .018),
          width: double.infinity,
          decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: buttonColor),
          ),
          child: const Center(
              child: Text(
            'Register Now',
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white, fontSize: 17),
          )),
        ),
      ),
    );
  }
}
