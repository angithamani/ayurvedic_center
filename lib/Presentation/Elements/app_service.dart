import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
bool loginLoader = false;
Future setCurrentUser(user) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString('current_user', json.encode(user));
  prefs.reload();
}

var userDetails;
Future getCurrentUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('current_user')) {
    userDetails = json.decode(prefs.getString('current_user')!);

    return userDetails;
  }
}
