

import 'dart:convert';

PatientsModel patientsModelFromJson(String str) => PatientsModel.fromJson(json.decode(str));

class PatientsModel {
  int id;
  List<PatientdetailsSet> patientdetailsSet;
  Branch branch;
  String user;
  String payment;
  String name;
  String phone;
  String address;
  dynamic price;
  int totalAmount;
  int discountAmount;
  int advanceAmount;
  int balanceAmount;
  DateTime dateNdTime;
  bool isActive;
  DateTime createdAt;
  DateTime updatedAt;

  PatientsModel({
    required this.id,
    required this.patientdetailsSet,
    required this.branch,
    required this.user,
    required this.payment,
    required this.name,
    required this.phone,
    required this.address,
    required this.price,
    required this.totalAmount,
    required this.discountAmount,
    required this.advanceAmount,
    required this.balanceAmount,
    required this.dateNdTime,
    required this.isActive,
    required this.createdAt,
    required this.updatedAt,
  });

  factory PatientsModel.fromJson(Map<String, dynamic> json) => PatientsModel(
    id: json["id"],
    patientdetailsSet: List<PatientdetailsSet>.from(json["patientdetails_set"].map((x) => PatientdetailsSet.fromJson(x))),
    branch: Branch.fromJson(json["branch"]),
    user: json["user"],
    payment: json["payment"],
    name: json["name"],
    phone: json["phone"],
    address: json["address"],
    price: json["price"],
    totalAmount: json["total_amount"],
    discountAmount: json["discount_amount"],
    advanceAmount: json["advance_amount"],
    balanceAmount: json["balance_amount"],
    dateNdTime: DateTime.parse(json["date_nd_time"]),
    isActive: json["is_active"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );
}

class Branch {
  int id;
  String name;
  int patientsCount;
  String location;
  String phone;
  String mail;
  String address;
  String gst;
  bool isActive;

  Branch({
    required this.id,
    required this.name,
    required this.patientsCount,
    required this.location,
    required this.phone,
    required this.mail,
    required this.address,
    required this.gst,
    required this.isActive,
  });

  factory Branch.fromJson(Map<String, dynamic> json) => Branch(
    id: json["id"],
    name: json["name"],
    patientsCount: json["patients_count"],
    location: json["location"],
    phone: json["phone"],
    mail: json["mail"],
    address: json["address"],
    gst: json["gst"],
    isActive: json["is_active"],
  );
}

class PatientdetailsSet {
  int id;
  int male;
  int female;
  int patient;
  int treatment;
  String treatmentName;

  PatientdetailsSet({
    required this.id,
    required this.male,
    required this.female,
    required this.patient,
    required this.treatment,
    required this.treatmentName,
  });

  factory PatientdetailsSet.fromJson(Map<String, dynamic> json) => PatientdetailsSet(
    id: json["id"],
    male: json["male"],
    female: json["female"],
    patient: json["patient"],
    treatment: json["treatment"],
    treatmentName: json["treatment_name"],
  );

}
