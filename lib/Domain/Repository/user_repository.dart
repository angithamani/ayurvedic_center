import 'dart:convert';

import 'package:http/http.dart' as http;

Future postLogin(userName, password) async {
  var data = {'username': userName, 'password': password};
  String url;
  url = "https://flutter-amr.noviindus.in/api/Login";
  final client = http.Client();
  final response = await client.post(
    Uri.parse(Uri.encodeFull(url)),
    body: data,
  );
  var jsonBody = json.decode(response.body);
  return jsonBody;
}

Future getPatientsList(user) async {
  String url;
  url = "https://flutter-amr.noviindus.in/api/PatientList";
  final client = http.Client();

  final response = await client.get(
    Uri.parse(url),
    headers: {
      "Authorization": "Bearer " + user['token'],
    },
  );

  var jsonBody = json.decode(response.body);
  return jsonBody;
}

Future getBranchList(user)async{
  String url;
  url = "https://flutter-amr.noviindus.in/api/BranchList";
  final client = http.Client();

  final response = await client.get(
    Uri.parse(url),
    headers: {
      "Authorization": "Bearer " + user['token'],
    },
  );
  var jsonBody = json.decode(response.body);
  return jsonBody;

}

Future getTreatmentList(user)async{
  String url;
  url = "https://flutter-amr.noviindus.in/api/TreatmentList";
  final client = http.Client();

  final response = await client.get(
    Uri.parse(url),
    headers: {
      "Authorization": "Bearer " + user['token'],
    },
  );


  var jsonBody = json.decode(response.body);
  return jsonBody;

}
